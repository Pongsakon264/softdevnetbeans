package com.mycompany.oxmavenproject;

import java.io.Serializable;
import java.util.*;

public class Player implements Serializable {

    private char name;
    private int win;
    private int lose;
    private int draw;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void win() {
        this.win++;
    }

    public void lose() {
        this.lose++;
    }

    public void draw() {
        this.draw++;
    }

}
