package com.mycompany.oxmavenproject;


import java.util.*;

public class Game {
	static Scanner kb = new Scanner(System.in);
	char turn;
	int count;
	int col;
	int row;
	Table table = null;
	Player O = null;
	Player X = null;

	public Game() {
		this.O = new Player('O');
		this.X = new Player('X');
	}

	public void run() {
		while (true) {
			this.runOnce();
			if (!askContinue()) {
				return;
			}
		}
	}

	private boolean askContinue() {
		while (true) {
			System.out.print("Continue Y/N ? : ");
			String ans = kb.next();
			if (ans.equals("N")) {
				return false;
			} else if (ans.equals("Y")) {
				return true;
			}

		}

	}

	private int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}

	public void newGame() {
		if (getRandomNumber(1, 100) % 2 == 0) {
			this.table = new Table(O, X);
		} else {
			this.table = new Table(X, O);
		}

	}

	public void runOnce() {
		this.showWelcome();
		this.newGame();
		this.table = new Table(O, X);
		while (true) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();
			if (table.checkWin()) {
				showResult();
				this.showStat();
				return;
			}

			table.switchPlayer();
		}

	}

	private void showResult() {
		if (table.getWinner() != null) {
			showWin();

		} else {
			showDraw();

		}
	}

	private void showDraw() {
		System.out.println("Draw!!!");
	}

	private void showWin() {
		System.out.println(table.getWinner().getName() + " Win!!!");
	}

	private void showStat() {
		System.out
				.println(O.getName() + " (win,lose,draw) : " + O.getWin() + " , " + O.getLose() + " , " + O.getDraw());
		System.out
				.println(X.getName() + " (win,lose,draw) : " + X.getWin() + " , " + X.getLose() + " , " + X.getDraw());
	}

	private void inputRowCol() {
		while (true) {
			this.input();
			try {
				if (table.setRowCol(row, col)) {
					return;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("Please input number 1-3");
			}

		}
	}

	private void showWelcome() {
		System.out.println("***** Welcome to OX GAME *****");
	}

	private void showTable() {
		char[][] data = this.table.getData();
		for (int row = 0; row < data.length; row++) {
			System.out.print("|");
			for (int col = 0; col < data[row].length; col++) {
				System.out.print(" " + data[row][col] + " |");
			}
			System.out.println("");
		}
	}

	private void showTurn() {
		System.out.println(table.getCurrentplayer().getName() + " " + "Turn");
	}

	private void input() {
		while (true) {
			try {
				System.out.print("Please input row col : ");
				this.row = kb.nextInt();
				this.col = kb.nextInt();
				return;
			} catch (InputMismatchException iE) {
				kb.next();
				System.out.println("Please input number 1-3");
			}
		}

	}

}